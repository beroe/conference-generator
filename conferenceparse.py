#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''In google sheet, sort abstracts by Session then by FINALFORMAT then Author.
Copy columns from Order over to FINALFORMAT into new document and save.
Run this with the TSV file path as argument.
    dsbsabstractparse.py AbstractsAug4.txt > Abstracts.html

You will need to customize several fields to match your conference. 
I've tried to mark these with ## ADJUST comments, but there are likely more

For updating the abstract book, open the html file which is generated
and copy the formatted text below the session definitions

Use -w to output wordpress format (slightly different CSS to match WP).
   Note the CSS for the wordpress may not be precisely what is on the site,
   so I've been using just the part below the menus.
   
Use -b for a little debugging info...
	
# TODO Example code: Way to access google sheet directly from within python
response = requests.get('https://docs.google.com/spreadsheet/ccc?key=xxxx&output=csv')
assert response.status_code == 200, 'Wrong status code'
print response.content

# FIXME There are a lot of things (Months, sessions) which are defined in multiple functions. 
# Need to move those out to global so all definitions are in one place.

(Apologies for the python 2.x)

https://bitbucket.org/beroe/conference-generator
'''

# TODO: Read directly from google sheet with google API. 

import os, sys
import argparse
import csv
import re

## Command-line options

def get_options():
	''' with no args, it prints the abstract book'''
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-w", "--wordpress",    action="store_true", default=False, help="Output with wordpress CSS")
	parser.add_argument("-b", "--debug",    action="store_true", dest="DEBUG", default=False, help="Debug output")
	parser.add_argument("-d", "--daily",    action="store_true", default=False, help="Print as daily schedule w/o abstracts. Similar to -t")
	parser.add_argument("-t", "--tabular",    action="store_true", default=False, help="Print daily schedule as table cells")
	parser.add_argument("-p", "--postersonly",    action="store_true", default=False, help="Print posters by themselves")
	parser.add_argument("-l", "--lightning",    action="store_true", default=False, help="Print lightning talk schedule")
	parser.add_argument("-e", "--email",    action="store_true", default=False, help="Print emails only")
	parser.add_argument("-s", "--student",    action="store_true", default=False, help="Print student poster numbers only")
	parser.add_argument("Args", nargs=argparse.REMAINDER)
	options = parser.parse_args()
	return options


def wordpress_css():
	Values = '''<style type="text/css">
		body { font-family: Helvetica, sans-serif; padding:6px 12px 8px 12px}
		div.talk  {color:#888}
		div.title { font-weight: bold; font-size: 18pt; }
		span.TOCtop {font-weight:bold; font-size:1.5em; }
		span.TOCsmall {color #444; font-size:0.8em; }
		div.TOC {padding:2px;}
	 	p.abs {font-size:small}
		p {margin-bottom:5px}
		div.session {font-size: 2em; border-bottom: double;
			border-top: double; border-color: #d84254; padding: 8px}
		div.author {font-weight: bold}
		div.affil {color: #888888; font-size:-2}
		hr.bl {border-color:#bdbfec; padding: 0px; margin-bottom: 5px; margin-top: 5px;}
		hr.dub {border-color:#d84254 ; border-style: double; padding-bottom: 12px}
		a.entry {color:#555; text-decoration: none; }
		a.entry:hover{ color:#d84254;}
	</style>'''
	
def printhead(wordpress=0):
	'''List with two strings containing different formats. 
	First entry is PDF style: second is for wordpress'''
	
	CSS = [# pdf style
"""<!DOCTYPE html>
<html lang="en">
	<style type="text/css">
/* 	pdf style */
		body { font-family: Helvetica, sans-serif; padding:6px 12px 8px 12px}
		div.sessiont {font-size: 1.3em; background-color: #d9e0fa; padding: 8px}
		div.sessionp {font-size: 1.3em; background-color: #abc9af; padding: 8px}
		div.talk  {color:#888; font-size:0.7em}
		div.timing  {color:#888; font-size:0.8em}
		div.title { font-weight: bold; font-size: 1em; }
		div.TOC {padding:2px;}
		span.TOCtop {font-weight:bold; font-size:1.5em; }
		span.TOCsmall {color #444; font-size:0.8em; }
	 	p.abs {font-size:0.7em}
		p {margin-bottom:5px}
		div.author {font-weight: normal; font-size:0.8em}
		div.affil {color: #888888; font-size:0.6em}
		hr.bl {border-color:#bdbfec; padding: 0px; margin-bottom: 5px; margin-top: 5px;}
		hr.dub {border-color:#d84254 ; border-style: double; padding-bottom: 12px}
		a.entry {color:#555; text-decoration: none; }
		a.entry:hover{ color:#d84254;}
	</style>""" ,
	
	#WORDPRESS STYLE 1
	"""<script type='text/javascript'>
window.addEventListener("hashchange", function () {
    window.scrollTo(window.scrollX, window.scrollY - 80);
});
</script>
<style type="text/css">
	/* 	Wordpress style */
	body { font-family: Helvetica, sans-serif; padding:6px 12px 8px 12px}
	div.talk  {color:#888}
	div.timing  {color:#888; font-size:0.8em}
	div.title { font-weight: bold; font-size: 1.3em;}
	span.TOCtop {font-weight:bold; font-size:1.5em; }
	span.TOCsmall {color #444; font-size:0.8em; }
	div.TOC {padding:6px 12px 8px 12px;}
	p.abs {font-size:small}
	div.session {font-size: 1.6em; font-color: #444; background-color: #d9e0fa; padding: 8px}
	div.sessiont {font-size: 1.6em; color: #222;  background-color: #d9e0fa; padding: 8px}
	div.sessionp {font-size: 1.6em; color: #222;  background-color: #abc9af; padding: 8px}
	div.author {font-weight: normal}
	div.affil {color: #888888; font-size:-2}
	hr.bl {border-color:#bdbfec}
	hr.dub {border-color:#d84254 ; border-style: double; padding-bottom: 12px}
	a.entry {color:#555; text-decoration: none; }
	a.entry:hover{ color:#d84254;}
</style>
""",  
# COLUMN CSS FORMAT 2
'''<html lang="en">
	<style type="text/css">
	/* 	timecolumn style */
		body { font-family: Helvetica, sans-serif; padding:12px 24px 12px 24px}
		div.subsession {text-transform:uppercase; font-size:.8em ; 
			background-color: #EEE; padding: 10px 0px 10px 6px}

		span.room  {color:#888; font-weight: normal; text-transform:uppercase; font-size:0.7em }
		#san_carlos{color:#600; font-weight: normal;}
		#serra{color:#006; font-weight: normal;}
		span.title  {font-size:1em; font-weight: bold;}
		div.event { 
		  padding-left: 42px ;
		  text-indent: -42px ;
		  padding-right: 12px;		
			font-weight: bold; 
			font-size: .9em; 
		}
	
		div.row { display: flex;}
		div.column { flex: 50%; padding: 0px 8px; }
	
		div.breakstyle {font-size: 1.2em; color: #222; padding: 12px 8px; background-color:#C9DFB5}
	
		div.day {font-size: 1.2em; color: #222;  background-color: #d9e0fa; padding: 8px}
		span.author {font-weight: normal; font-size:0.7em}
	
		hr.bl {border-color:#bdbfec; padding: 0px; margin-bottom: 5px; margin-top: 5px;}
	</style>

''' ,
# The rest of the document header 3
''' 
<!DOCTYPE html>
<html lang="en">
	<style type="text/css">
	/* 	tableformatted style */
	*{font-size:10pt;}
	
	/* Items for session TOC */
	.TOCtop {font-weight:bold; font-size:1.5em; text-align: center;}
	.TOCsmall {color #444; font-size:0.8em; text-align: center;}
	div.TOC {padding:12px 0px;}
	hr.bl {border-color:#bdbfec; padding: 0px; align-items: flex-start ; margin-bottom: 5px; margin-top: 5px; width: 90%;}
	.tochead {
		padding: 6px 8px;
		text-transform: uppercase;	
	}
	
	.tocrow {
		padding: 18px 12px;
		border-bottom: white solid 2px;
		vertical-align: middle;
	}
	
	.tabletocsession {
		padding: 8px 12px;
		font-weight: bold;
		font-size: 1.1em ; 
/* 		color: #4a7899; */
	}
	.tabletocdays {
		padding: 8px 12px;
		font-size: 1 em ; 
	}
	
	.tableroom {
		padding: 8px 12px;
		text-transform: uppercase;
		font-size: .8 em ; 
		width: 20%;
	}

	
	body { 
		font-family: Helvetica, sans-serif; 
/* 		padding:12px 24px 12px 24px; */
	
		}
			/* TODO: every other row border 
				fix padding on times
				make sure about cascading text sizes
			*/
		
	table {
		border-collapse: collapse;
	    width: 90%;
	    background-color: #fff;
	}

	td.ttalk {
	    padding: .6em 1em .4em .6em;
	    text-align: left;
	    border-color: #c0bee9;
	    border: 1px;
/* 	    vertical-align: top; */
	 }
    
    td.time_san{
	    color:#222;
/* 	    border-left: 4px solid white */
/*	    border-left: 2px solid rgba(129,126,154,0.1)    */
	}

	tr {
		vertical-align: top;
		border-bottom: 1px solid #ddd;
	}

/*
	tr:nth-child(even) {
		background-color: #f1f1fa;
	}
*/



	.tday {content-align: left; 
		color:#eee; 
		font-weight: normal;
		padding: 24px; 
		font-size: 1.8em;
		text-align: center;
		background-color: #4a7899;
		letter-spacing:0.25em;
		/*background-color: #d9e0fa*/
	}

	td.tevent { 
		padding: 1em .8em;
		font-weight: bold; 
/* 			vertical-align: middle; */
		border-bottom: 2px solid #fff;
	}
	td.etime {
		padding-right: .3em; 
		padding-top: 1em;
		width: 8%;
		text-align: right;
		font-weight: normal;
		border-bottom: 2px solid #fff;
	}

	 .tevent.event_bg{ background-color: #e3f2d5; }
	 .tevent.eventp_bg{	background-color: #ecd7ef; }

	.subsession {
		text-transform:uppercase; 
		font-size: 1.1em ; 
		padding: .9em .6em .8em 1em;
		/* top right bottom left */
		/*border-bottom: solid 10px rgba(0,0,0,.1); */
	}
	.subgap{
		padding-top:.5em;
	}

	.ttalk{
		padding-top: .85em;
		width: 40%;
		padding-bottom: .4em;
		padding-top: .8em;
	}

	.dtitle  {
		/* space before*/
		font-size:9pt; 
		font-weight:bold;
		color: #4a7899;
	}

	.dauthor{
		padding: .5em 0em 0em 0em;
		font-size: 7pt;
		font-weight: normal;
	}
	span.firstauthor{
		font-size: 7pt;
		font-weight: bold;
	
	}

	span.otherauthors{
		font-size: 7pt;
		font-weight: normal;
	
	}

	.ttime {
		padding-right: .3em; 
		padding-top: 1.2em;
		width: 8%;
		text-align: right;
		font-weight: normal;
	}
	/*
	.ttime:first{
		padding-top: 2em;
	}
	*/

	.san_carlos {background-color: #EBCFAA;
		 color:#222; }
	.serra {background-color: #C0BFD3;
		color: #222; }
	.textserra{color:#4a7899;}
	.textsan_carlos{color:#6b4a33;}
	
	</style>	

''' ]  # END header
	
	Top = ['''
	<head> <meta charset="utf-8" /> 
		<title>DSBS Abstracts</title> 
	</head>
<body>
''',"",'''
	<head> <meta charset="utf-8" /> 
		<title>DSBS Timetable</title> 
	</head>
	<body>
''','''
	<head> <meta charset="utf-8" /> 
		<title>DSBS Timetable</title> 
	</head>
	<body>
''']

	print CSS[wordpress] + Top[wordpress]

def printTOC(postersonly=False):
	
	import datetime as dt

	SessionList = ["Advances in taxonomy and phylogenetics",
		"Autecology","Biodiversity and ecosystem functioning","Chemosynthetic ecosystems",
		"Connectivity and biogeography","Corals","Deep-ocean stewardship","Deep-sea omics",
		"James J. Childress","Mining impacts","Natural and anthropogenic disturbance",
		"Pelagic systems","Seamounts and canyons","Technology and observing systems","Trophic ecology"]
	ShortSessionList = [
		"Taxonomy","Autecology","Biodiversity","Chemosyn.","Connectivity",
		"Corals","Stewardship","Omics","Childress","Mining","Disturbance",
		"Pelagic","Seamounts","Technology","Trophic"]
	SessionDict = dict(zip(SessionList,ShortSessionList))
	
	Months=["NA","Jan","Feb","Mar","Apr","May","Jun",
		"Jul","Aug","Sep","Oct","Nov","Dec"]

	i = dt.datetime.now()
	UpdateString = "-".join([str(i.day), Months[i.month],str(i.year)])
	# Easier time? time.strftime("%d-%b-%Y",time.localtime())

	print '''<div class="TOC" id="toc"><span class="TOCtop">Abstracts &bullet; Deep-Sea Biology Symposium 2018 </span><br/>
	<span class="TOCsmall">Updated: {} &bullet; <a href="http://dsbs2018.org">Symposium Page</a></span><br/>
	<hr class="bl"/>'''.format(UpdateString)
	
	for Session in SessionList:
		AnchorBase = Session.split(" ")[0].lower()
		if postersonly:
			print '''<a class="entry" href="#{0}P">{1} </a>'''.format(AnchorBase,Session)
		else:
			print '''<a class="entry" href="#{0}T">{1} </a>'''.format(AnchorBase,Session),
			for Format in ("TALKS","POSTERS"): 
				AnchorName = AnchorBase + Format[0]
				print '''<a class="entry" href="#{0}"> &bullet; {1}</a>'''.format(AnchorName,Format),
		print '<hr class="bl"/>'
	print "</div>"
	print "\n\n<!-- end session TOC -->\n\n"

def printTOCtable():
	''' Generate the timetable as shown in the image preview 
	Edit session list and short session names below'''
	import datetime as dt

	## ADJUST
	SessionList = ["Advances in taxonomy and phylogenetics",
		"Autecology","Biodiversity and ecosystem functioning","Chemosynthetic ecosystems",
		"Connectivity and biogeography","Corals","Deep-ocean stewardship","Deep-sea omics",
		"James J. Childress","Mining impacts","Natural and anthropogenic disturbance",
		"Pelagic systems","Seamounts and canyons","Technology and observing systems","Trophic ecology"]
	ShortSessionList = [
		"Taxonomy","Autecology","Biodiversity","Chemosyn.","Connectivity",
		"Corals","Stewardship","Omics","Childress","Mining","Disturbance",
		"Pelagic","Seamounts","Technology","Trophic"]
	SessionDict = dict(zip(SessionList,ShortSessionList))
	
	## ADJUST
	# Shortname: roomcss, Session, day, room, chairs
	
	SessionDays = {
	"Taxonomy" 		: ["san_carlos","Tuesday midday","San Carlos","Allen Collins, Lonny Lundsten"],
	"Autecology" 	: ["serra","Thursday afternoon","Serra","Janet Voight, Fanny de Busserolles"],
	"Biodiversity" 	: ["san_carlos","Monday midday and afternoon, Tuesday afternoon","San Carlos","Moriaki Yasuhara, Paris Stefanoudis"],
	"Chemosyn." 	: ["serra","Monday midday and afternoon","Serra","Shana Goffredi, Diva Amon"],
	"Connectivity" 	: ["san_carlos","Thursday morning, midday, afternoon","San Carlos","Shannon Johnson, Ana Hilario"],
	"Corals" 		: ["serra","Thursday afternoon and Friday morning","San Carlos and Serra","Steve Litvin, Santiago Herrera, Branwen Williams"],
	"Stewardship" 	: ["san_carlos","Friday morning","San Carlos","Maria Baker, Lisa Levin, Susan von Thun"],
	"Omics" 		: ["serra","Monday morning and midday","Serra","Andrea Quattrini, Franck Lejzerowicz, Santiago Herrera"],
	"Childress" 	: ["san_carlos","Wednesday morning", "San Carlos","Erik Thuesen, Brad Seibel"],
	"Mining" 		: ["serra","Tuesday midday and afternoon","Serra","Adrian Glover, Craig Smith, Daphne Cuvelier"],
	"Disturbance" 	: ["serra","Tuesday afternoon and Wednesday morning","Serra","Verena Tunnicliffe, Chris Yesson, Katherine Dunlop"],
	"Pelagic" 		: ["serra","Friday afternoon","Serra","Tracey Sutton, Dhugal Lindsay"],
	"Seamounts" 	: ["san_carlos","Monday morning and midday" ,"San Carlos ","Malcolm Clark, Tina Molodtsova"],
	"Technology" 	: ["serra","Thursday morning and midday","Serra","Brian Kieft, Alana Sherman, Luciana Genio, Amanda Netburn"],
	"Trophic" 		: ["serra","Friday midday","Serra","Anela Choy, Rachel Jeffreys"] }
	
	Months=["NA","Jan","Feb","Mar","Apr","May","Jun",
		"Jul","Aug","Sep","Oct","Nov","Dec"]

	i = dt.datetime.now()
	UpdateString = "-".join([str(i.day), Months[i.month],str(i.year)])
	# Easier time? time.strftime("%d-%b-%Y",time.localtime())

	## ADJUST Change the conference name here
	print '''<div class="TOC" id="toc"><div class="TOCtop">Session List &bullet; Deep-Sea Biology Symposium 2018 </div><br/>
	<div class="TOCsmall">Updated: {} &bullet; <a href="http://dsbs2018.org">Symposium Page</a></div><br/><br/>
	'''.format(UpdateString)
	
	SessionList.sort()
	print '<table>'
	print '''<tr class="tochead">
	<th class="tochead">Session</td>
	<th class="tochead">Day and Time Period</td>
	<th class="tochead">Co-chairs</td>
	<th class="tochead">Location</td>
	</tr>'''
	
	for Session in SessionList:
		tocstyle,days,room,chairs = SessionDays.get(SessionDict.get(Session))
		
		print '''<tr class="tocrow {0}">
		<td class="tabletocsession">{1}</td>
		<td class="tabletocdays">{2}</td>
		<td class="tabletocdays">{4}</td>
		<td class="tableroom">{3}</td>
		</tr>'''.format(tocstyle,Session,days,room,chairs)
	print "</table>"
	print "\n\n<!-- end table TOC -->\n\n"
	print '''
	<div class="TOC" id="toc"><br><span class="TOCtop">Daily Schedule of Talks and Events</span><br/><br/>
	'''.format(UpdateString)
	

def printtail():
	Tail = """
</body>
</html>	"""
	print Tail


def printsession(SessionName,theformat,wordpress=False):
	AnchorName = SessionName.split(" ")[0].lower() + theformat[0]
	# Don't print [TOP] links unless --wordpress
	TopLink= ['    ','<a href="#toc" style="font-size: small; color:#888;">[TOP]</a>'][wordpress]
	TString = '''<div class="session{}" id="{}"><b>{}</b> - {}S   {}\t</div>
<br/>'''.format(theformat[0].lower(),AnchorName,SessionName,theformat,TopLink)
	print TString


def printtalk(Fields,postersonly,lightning,):
	AuthorBreak = ['<br>',''][postersonly or (lightning > 0)]
	FirstFields = ['{myformat} {bullet} '.format(**Fields),''][postersonly or (lightning > 0)]
	TimingFields = ['	<div class="timing">{day} {timeblock} • {mytime} • {room}  </div>'.format(**Fields),''][postersonly]
	if postersonly or (Fields['myformat']=='POSTER'):
		FirstLine = '''<div class="talk">POSTER {} {bullet} {sessionname} </div>
		{}'''.format(lightning,TimingFields,**Fields)
	else:
		FirstLine = '''<div class="talk">{}{sessionname} {bullet} ABSTRACT {order} </div>
		{}'''.format(FirstFields,TimingFields,**Fields)

	# Hack to get it to print this field — breaks lightning
	
	if postersonly or True:
		print '''
		{}
		<div class="title" style="color:{stylecolor}">{mytitle}</div>
		<div class="author">{}{authors}</div>'''.format(FirstLine,AuthorBreak,**Fields)
	else: # is lightning
		FirstFields=lightning
		thename = cleannames(Fields,True)
		print '''
		{}
	<span class="author">{authorname}</span> • 
	<span class="title" style="color:{stylecolor}">{mytitle}</span>'''.format(FirstLine,FirstFields,**Fields)
		
	if not (postersonly):
		print '''<div  class="affil">{affil}</div>
	<p class="abs">{abstract}</p>'''.format(**Fields)
		print '''<hr class="bl"/>'''

def printdayhead(Day):
	print '<hr class="bl"/>'
	TString = '''<div class="day"><b>{}</b></div> <br/>'''.format(Day)
	print TString

	
def printevent(Fields):
	
	''' Color time and room by color '''
	# TODO: SHort session names...
	if Fields["room"]=="OTHER":
		print '''<div class="breakstyle">{mytime} • <span class="title">{mytitle}</span></div>
		<br/>'''.format(**Fields)
	else:
		#REMOVED SPAN INFO WITH HTML COMMENT...
		# 		<span class="room" id="{sessionstyle}">  • {shortsession} • {room} <br/></span>

		print '''
		<div class="event">{mytime} • <span class="title">{mytitle}</span><br>
		<span class="author">{authors}</span>
		</div>
		<br/>
	'''.format(**Fields)

def printcolumnstart():
	print '''<div class="row">
	  <div class="column">'''

def printcolumnnext():
	print '''</div>
	  <div class="column">'''
		
def printcolumnend():
	print '''</div> <!--endcol-->
	</div><!--endrow-->'''
	
	
def printtablestart():
	print '''<tr class="ttalk">
	  '''
	  
def printtableevent(L):
	# need to make tevent .break and .plenary bg colors
	eventtype = L['finalformat'].lower()
	print 	'''<tr class="tevent {0}_bg">
	<td class="etime">{mytime}</td>
	<td class="tevent {0}_bg" colspan="3">{mytitle}</td>
</tr>'''.format(eventtype,**L)
	
def returntablesession(L):
	# add tevent styles for plenary vs break
	# Style session background by room color
	# Return the format... Make this just a span inside the cell? 
	# div.subsession {text-transform:uppercase; font-size:.8em ; 
	#	 background-color: #EEE; padding: 10px 0px 10px 6px}
	sessionstyle = L['room'].lower().replace(" ","_")
	OutString = '''<div class="subsession {}" colspan="2">{} • {} \t</div>
	<div class="subgap"> <div>\n'''.format(
	    sessionstyle, L['shortsession'],L['room'])
	return OutString

def printtabletalk(L,TalkRoom,SessionString):
	'''print two td for talk times. 
	Eventually Use different style for left and right.
	NEED TO FIGURE SESSION: Own table rows, and what about parallel'''
	if TalkRoom=="Serra":
		OpenString = '<tr class="ttalk">'
		timeclass = ''
	else:
		OpenString = ''
		timeclass = ' time_san'
		
	TalkString = '''	<td class="ttime{1}">{mytime}</td>
	<td class="ttalk">{0}<div class="dtitle">{mytitle}</div>
		<div class="dauthor">{authors}</div>
		</td>'''.format(SessionString,timeclass,**L)

	## ADJUST
	# This section was used to transition from parallel sessiont to single session at the end
	# of the conference
	# Make this different for the last two sessions ==> if time > some value....
	# Time > 500.5 or blocktime > 520.5
	CloseString = ['</tr>',''][(TalkRoom=="Serra" and L['timetot'] > 500.5)]
	print OpenString + TalkString + CloseString
			
def printtableend():
	print '''</tr> <!-- talkrow -->
	'''
def printtableday(Day):
	# Added this to make a new table for each day...
	print '''</table><table>'''
	print '''<tr class="event"><td class="tday" colspan="4">{}</td>
	</tr>'''.format(Day)

def cleannames(row,lightning=False):
	authorRE = re.compile(r'[\(\d\)\]\[\*]+')
	if row['authorname']:
		firstauthor = authorRE.sub("",row['authorname'])
		firstauthor = firstauthor.replace(" ,",",").replace(",","").replace("*","")
		if lightning:
			cleanauthors = firstauthor
		else:
			cleanauthors = '<span class="firstauthor">'+ firstauthor + '</span><span class="otherauthors">' + ['',', '][len(row['coauthors'])>4] + row['coauthors']+'</span>'	

		cleanauthors = authorRE.sub("",cleanauthors)
		cleanauthors = cleanauthors.replace(" ,",",").replace(",,",",").replace(",,",",").replace("*","")

	else:
		cleanauthors = ""
	return cleanauthors
	
def parsedailytable(reader,Options):
	'''print abstractbook format'''
	## ADJUST FIELD ORDER names
	'''FieldNames=["order","prefix","firstname","lastname","authorname","affil",
	"coauthors","mytitle","abstract","sessionname","newformat","finalformat",
	"jobtitle","preferredformat","lightning","finallightning","day","timeblock","room","mytime",'daynum','timetot','blocknum','blocktotal']
	'''
	## ADJUST DAYS AND DATES (Abbreviations from sheet to full dates)
	dayD = {"MON" : "MONDAY • 10th","TUE" : "TUESDAY • 11th",
	"WED" : "WEDNESDAY • 12th","THU" : "THURSDAY • 13th",
	"FRI" : "FRIDAY • 14th"}
	LineD = {}
	
	## ADJUST session names
	SessionList = ["Advances in taxonomy and phylogenetics",
		"Autecology","Biodiversity and ecosystem functioning","Chemosynthetic ecosystems",
		"Connectivity and biogeography","Corals","Deep-ocean stewardship","Deep-sea omics",
		"James J. Childress","Mining impacts","Natural and anthropogenic disturbance",
		"Pelagic systems","Seamounts and canyons","Technology and observing systems","Trophic ecology"]
	ShortSessionList = [
		"Taxonomy","Autecology","Biodiversity","Chemosyn.","Connectivity",
		"Corals","Stewardship","Omics","Childress","Mining","Disturbance",
		"Pelagic","Seamounts","Technology","Trophic"]
		
	SessionDict = dict(zip([x.lower() for x in SessionList],ShortSessionList))
	
	
	for row in reader:
		try:
			decimaltime = float(row['timetot'])
		except ValueError:
			decimaltime = 0.1
		LineKey = (decimaltime,row['room'])
		# LineKey = (row['day'],row["timeblock"],row['room'],decimaltime)
		row['blocktot'] = decimaltime
		row['day'] = dayD.get(row['day'[:3]],row['day'])
		
		row['authors'] = cleannames(row)
		
		row['shortsession'] = SessionDict.get(row['sessionname'].lower(),row['sessionname'])

		LineD[LineKey] = row
		
	LineKeys = LineD.keys()
	from operator import itemgetter
	LineKeys.sort(key=itemgetter(1),reverse=True)
	LineKeys.sort(key=itemgetter(0))
	# LineKeys.sort()
	NewDay = ""

	printhead(3)  # new table header...
	# TODO: Figure room colors to match color abstract BREAKS too.

	
	printTOCtable()
	
	## ADJUST Which session gets the left column
	InDouble = False
	TalkRoom = "Serra"
	NowSession = {'Serra':"Nada","San Carlos":"Nil"}
	ColumnNumber = 0

	
	print '''
	<table  cellpadding = "4" bgcolor="#EEE">
		
	'''
	
	for K in LineKeys:
		L = LineD[K]
		
		truncform = L['finalformat'][:3]
		
		if K[0] > 1: # timefield is not a poster
			if L['day'] != NewDay:
				if InDouble:
					printtableend()
					InDouble = False
				printtableday(L['day'])
				NowSession = ""
				NewDay = L['day']
			# changing from single to double

			L['sessionstyle'] = L['room'].lower().replace(" ","_")
			
			if truncform != "TAL":
				if InDouble:
					printtableend()
				printtableevent(L) #colspan 3
				ColumnNumber = 0
				InDouble = False
				TalkRoom = ""
				NowSession = {}	
			else:
				# print session first
				# Don't need InDouble? Just print based on room?
				TalkRoom = L['room']
				if NowSession.get(TalkRoom,'') != L['sessionname']:
					NowSession[TalkRoom] = L['sessionname']
					SessionString = returntablesession(L)
				else:
					SessionString = ""
				printtabletalk(L,TalkRoom,SessionString)
				InDouble = True
			
		elif truncform == "TAL":
			print >> sys.stderr, "ERROR in timetable?",L
	print '''	
	</table> 
	'''
	printtail()
	
def parsedaily(reader,Options,dayD):
	'''print abstractbook format'''
	'''FieldNames=["order","prefix","firstname","lastname","authorname","affil",
	"coauthors","mytitle","abstract","sessionname","newformat","finalformat",
	"jobtitle","preferredformat","lightning","finallightning","day","timeblock","room","mytime",'daynum','timetot','blocknum','blocktotal']
	'''
	
	LineD = {}
	
	## ADJUST (UGH here is the session list again)
	SessionList = ["Advances in taxonomy and phylogenetics",
		"Autecology","Biodiversity and ecosystem functioning","Chemosynthetic ecosystems",
		"Connectivity and biogeography","Corals","Deep-ocean stewardship","Deep-sea omics",
		"James J. Childress","Mining impacts","Natural and anthropogenic disturbance",
		"Pelagic systems","Seamounts and canyons","Technology and observing systems","Trophic ecology"]
	ShortSessionList = [
		"Taxonomy","Autecology","Biodiversity","Chemosyn.","Connectivity",
		"Corals","Stewardship","Omics","Childress","Mining","Disturbance",
		"Pelagic","Seamounts","Technology","Trophic"]
		
	SessionDict = dict(zip([x.lower() for x in SessionList],ShortSessionList))

	
	for row in reader:
		try:
			decimaltime = float(row['blocktotal'])
		except ValueError:
			decimaltime = 0.1
		LineKey = (decimaltime,row['room'])
		# LineKey = (row['day'],row["timeblock"],row['room'],decimaltime)
		row['blocktot'] = decimaltime
		row['day'] = dayD.get(row['day'[:3]],row['day'])
		
		row['authors'] = cleannames(row)
		
		row['shortsession'] = SessionDict.get(row['sessionname'].lower(),row['sessionname'])

		LineD[LineKey] = row
		
	LineKeys = LineD.keys()
	LineKeys.sort()
	NewDay = ""
	printhead(2)
	# TODO: Figure room colors to match color abstract BREAKS too.

	## ADJUST SESSION ROOMS FOR LEFT AND RIGHT COLUMNS
	InDouble = False
	TalkRoom = "Serra"
	NowSession = "Nada"
	
	for K in LineKeys:
		L = LineD[K]
		
		truncform = L['finalformat'][:3]
		
		if K[0] > 1: # timefield is not a poster
			if L['day'] != NewDay:
				if InDouble:
					printcolumnend()
					InDouble = False
				printdayhead(L['day'])
				NowSession = ""
				NewDay = L['day']
			# changing from single to double
			
			L['sessionstyle'] = L['room'].lower().replace(" ","_")
			
			if not InDouble and truncform == "TAL":
				printcolumnstart()
				InDouble = True
				TalkRoom = L['room']
		
			# within double changing to new column
			elif InDouble and truncform == "TAL" and TalkRoom != L['room']:
				printcolumnnext()
				TalkRoom = L['room']
		
			# changing from double to single column
			elif InDouble and truncform != "TAL": 
				printcolumnend()
				InDouble = False
				
			if InDouble and NowSession != L['sessionname']:
				print '''<div class="subsession" id="{}">{} • {}</div>'''.format(L['sessionstyle'],L['sessionname'],L['room'])
				NowSession = L['sessionname']
				
			printevent(L)
			
		elif truncform == "TAL":
			print >> sys.stderr, "ERROR in timetable?",L
	printtail()
		

		

if __name__ == "__main__": 
	
	Options = get_options()
	global DEBUG
	DEBUG = Options.DEBUG
	if DEBUG:
		print >> sys.stderr, "DEBUG MODE..."
		print >> sys.stderr, Options
	if not Options.Args:
			sys.exit(__doc__)
	else: # give a way to run on specified directories...
		if DEBUG:
			print >> sys.stderr, Options.Args
		FileName = Options.Args[0]
		outstring = ""
		Count = {'TALK':0,'POSTER':0,'lightning':0,'priority':0}
		lightningnum = 0
		LightningCount = False
		
		if Options.student:
			Options.postersonly = True
			
		NowSession = ""
		NowFormat = ""

		AlreadyDid = True
		
		# In order of Poster,Talk 
		FormatColor = ["#25603f","#3b3d7a"]  #unused: superseded by CSS
		
		BulletSymbol = ['-','&bullet;','+']
		FormD = {'POS':'POSTER','TAL':'TALK'}
		dayD = {"MON" : "MONDAY","TUE" : "TUESDAY",
"WED" : "WEDNESDAY","THU" : "THURSDAY",
"FRI" : "FRIDAY"}

		## ADJUST  guess what... put fields here
		FieldNames=["order","prefix","firstname","lastname","authorname","affil",
		"coauthors","mytitle","abstract","sessionname","newformat","finalformat",
		"jobtitle","preferredformat","lightning","finallightning","day","timeblock","room","mytime",
		'daynum','timetot','blocknum','blocktotal','student']
		
		FStream = open(FileName,'rU')
		reader = csv.DictReader(FStream, fieldnames=FieldNames, delimiter='\t')
			
		if Options.daily:
			parsedaily(reader,Options,dayD)
		elif Options.tabular:
			parsedailytable(reader,Options)
			print >> sys.stderr, "COPY from Web preview to keep most formatting"
			
		else:
			printhead(Options.wordpress) # put True to print wordpress style
			if not Options.lightning:
				printTOC(Options.postersonly)
				
			for row in reader:
				LightningCount = False
				truncform = row['finalformat'][:3]
				theformat = FormD.get(truncform,"")
				
				if Options.postersonly or Options.lightning:
					row['authors'] = cleannames(row)
					row['myformat'] = ''
				else:
					row['authors'] = row['authorname'] + ['',', '][len(row['coauthors'])>4] + row['coauthors']

					if row['authorname'] and (row['authorname'][:7] == row['coauthors'][:7]):
						print >> sys.stderr, "Author Overlap:", row['order'],row['authors']
			
					if row['authorname'] and len(row['affil']) < 2:
						print >> sys.stderr, "Affil Missing :", row['order'],row['authors']
			
			

					# TODO: convert italics in title and abstract
			
					# print row SESSION header for each new session

				if (((row['sessionname'] != NowSession) or (theformat != NowFormat)) and theformat):
					if ( not Options.postersonly or (Options.postersonly and theformat == "POSTER")):
						if ( not Options.lightning or (Options.lightning and theformat == "POSTER")):
							printsession(row['sessionname'],theformat,Options.wordpress)
					NowSession = row['sessionname']
					NowFormat  = theformat
					FirstTime = False
				
				wantlightning = row['lightning'] and row['lightning'][0].upper()=='Y'
				row['myformat'] = theformat
				LightningCandidate = (theformat == 'POSTER') and (wantlightning == True)
				LightningPriority = LightningCandidate and (row['finalformat'][:7] == "POSTERt") and (row['finallightning'][:3] not in ("CON","YES"))
				row['bullet'] = BulletSymbol[LightningCandidate + LightningPriority]
 	# extra field for confirming lightning talk acceptance
				if (row['finallightning'][:4] == "CONF") or (row['finallightning'][:3] == "YES"):
					row['bullet'] = '&#9889;'
					if Options.lightning:
						LightningCount = True
						row['myformat'] = 'POSTER'
						theformat = 'POSTER'
				else:
					LightningCount = False
				if row['myformat']:
					row['stylecolor'] = FormatColor[theformat == 'TALK']
					if theformat == 'POSTER':
						row['day'] = "TUESDAY"
						lightningnum += 1
						if (row['finallightning'][:4] == "CONF") or (row['finallightning'][:3] == "YES"):
							row['room'] = "Serra Room"
							row['mytime'] = "08:30"
							row['timeblock']= "Lightning Talks"
						
						else:
							row['room'] = "Serra Ballroom"
							row['mytime'] = "17:45"
							row['timeblock']= "Evening"
					else:
						row['room'] += ' Room'
						row['day'] = dayD.get(row['day'[:3]],row['day'])
					if Options.DEBUG:
						print >> sys.stderr, "lightningnum:",lightningnum
					if (not Options.lightning and (not Options.postersonly) or (Options.postersonly and theformat == 'POSTER')) or \
						(Options.lightning and LightningCount):
							if ( not Options.student or (Options.student and row['student'] == "Y")):
								printtalk(row,Options.postersonly,lightningnum) # changed lightning here...
					if not Options.lightning and theformat:
						Count[theformat] = Count[theformat] + 1
						# Count['lightning'] = Count['lightning'] + 1 * LightningCount
						# Count['priority'] = Count['priority'] + 1 * LightningPriority
			if not Options.wordpress:
				printtail()
		
			if not Options.lightning:
				print >> sys.stderr, Count
		FStream.close()





