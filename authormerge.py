#!/usr/bin/env python
'''
given a file of authors and affiliations, separated into first name, last name, affiliation.
pasted into a text document without headers (can leave headers and delete them at the end)

It goes through and merges the author first and last names and the affiliations '''


Filename = sys.argv[1]

with open(Filename) as fi:
	for L in fi:
		firstfield = L.rstrip("\n").split("\t")
		field = firstfield[1:]
		Merged = field[0] + " " + field[1] + ", " + field[2] + "; " +  \
			field[3]  + " " + field[4]  + ", " + field[5]  + "; " +  \
			field[6]  + " " + field[7]  + ", " + field[8]  + "; " +  \
			field[9]  + " " + field[10] + ", " + field[11] + "; " +  \
			field[12] + " " + field[13] + ", " + field[14] + "; " +  \
			field[15] + " " + field[16] + ", " + field[17] + "; " +  \
			field[18] + " " + field[19] + ", " + field[20] + "; " +  \
			field[21] + " " + field[22] + ", " + field[23] + "; " +  \
			field[24] + " " + field[25] + ", " + field[26] + "; " +  \
			field[27] + " " + field[28] + ", " + field[29] + "; " +  \
			field[30]
		MergeTrim = Merged.replace(" , ; ","")
		MergeTrim.replace("  "," ")
		print MergeTrim.rstrip(" ").rstrip(";")
	
