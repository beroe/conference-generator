#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''In the google sheet, sort abstracts by Session then FINALFORMAT then Author.

Copy columns from Firstname over to FINALFORMAT into new document and save.
Run this with the TSV file path as argument.
    edsbs_parse.py AbstractsAug4.txt > Abstracts.html

for updating the abstract book, open the html file and copy the formatted
text below the session definitions
For updating the wordpress site, use -w
   Note the CSS for the wordpress may not be precisely what is on the site,
   so I've been using just the part below the menus.

Use -b for a little debugging info...

It can output schedules with different time zones, assuming the database time is GMT. Check offsets for daylight savings, etc

Fields in spreadsheet are are: Author	Affiliation	ShortTime	ID	Early-CareerAward	StudentAward	MergedCoauthors	Title	Abstract	FinalSession	AssignedFormat	AssignedTimeSlot	AssignedDate	DAYNUM	TIMETOT	-7	-4	9

MergedCoauthors field includes affiliations. You can potentially generate this with the authormerge.py utility script

REF: https://bitbucket.org/beroe/conference-generator/
'''
# Version 3. Modified for eDSBS 2020 by SH
# Version 2  Modified for eDSBS by Santiago Herrera 2019
# Version 1  For DSBS by Steve Haddock 2018

import os, sys
import argparse
import csv
import cgi
import re

## ADJUST session names and short names here

SessionList = [
	"Advances in taxonomy and phylogenetics",
	"Biodiversity and ecosystem functioning",
	"Chemosynthetic ecosystems",
	"Connectivity and biogeography",
	"Corals",
	"Deep-ocean stewardship",
	"Deep-sea omics",
	"Natural and anthropogenic disturbance",
	"Pelagic systems",
	"Seamounts and canyons",
	"Trophic ecology"]

ShortSessionList = [
	"Taxonomy"       ,
	"Biodiversity"   ,
	"Chemosyn."      ,
	"Connectivity"   ,
	"Corals"         ,
	"Stewardship"    ,
	"Omics"          ,
	"Disturbance"    ,
	"Pelagic"        ,
	"Seamounts"      ,
	"Trophic"         ]

SessionDict = dict(zip(SessionList,ShortSessionList))

def get_options():
	''' run with no args gives abstract book '''
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-w", "--wordpress",    action="store_true", default=False, help="Output with wordpress CSS")
	parser.add_argument("-b", "--debug",        action="store_true", default=False, dest="DEBUG", help="Debug output")
	parser.add_argument("-t", "--tabular",      action="store_true", default=False, help="Print daily schedule as table cells")
	parser.add_argument("--pacific",      action="store_true", default=False, help="Print Pacific schedule")
	parser.add_argument("--eastern",      action="store_true", default=False, help="Print Eastern schedule")
	parser.add_argument("--japan",      action="store_true", default=False, help="Print Japan schedule")
	parser.add_argument("--difftime", action="store_true",help="Placeholder for detecting alt time zone request")
	parser.add_argument("Args", nargs=argparse.REMAINDER)
	options = parser.parse_args()
	return options

def wordpress_css():
	Values = '''<style type="text/css">
		body { font-family: 'Open Sans', sans-serif; padding:6px 12px 8px 12px}
		div.talk  {color:#888; font-size:0.9em}
		div.title { font-weight:Medium; font-size: 1.2em; }
		span.TOCtop {font-weight:Medium; font-size:1.6em; }
		span.TOCsmall {color #444; font-size:0.9em; }
		div.TOC {padding:2px;}
	 	div.abs {color:#5c5c5c; font-size:0.9em}
		p {margin-bottom:5px}
		div.session {font-size: 2em; border-bottom: double;
			border-top: double; border-color: #d84254; padding: 8px}
		div.author {color:#000000; font-weight: normal; font-size:1em}
		div.affil {color: #888888; font-size:0.9em}
		hr.bl {border-color:#bdbfec; padding: 0px; margin-bottom: 5px; margin-top: 5px;}
		hr.dub {border-color:#d84254 ; border-style: double; padding-bottom: 12px}
		a.entry {color:#555; text-decoration: none; }
		a.entry:hover{ color:#FFDA00;}
	</style>'''

def printhead(wordpress=0):
	'''List of four specific styles fields. First entry is PDF style: second is for wordpress, 
	You can edit the CSS here to change the color schemes. '''

	CSS = [# pdf style
"""<!DOCTYPE html>
<html lang="en">
	<style type="text/css">
/* 	pdf style */
		body { font-family: 'Open Sans', sans-serif; padding:6px 12px 8px 12px}
		div.sessiont {color:#ffffff; font-size: 1.4em; background-color: #83a4d1; padding: 8px}
		div.sessionp {color:#ffffff; font-size: 1.4em; background-color: #eb9e96; padding: 8px}
		div.talk  {color:#888; font-size:0.9em}
		div.timing  {color:#888; font-size:0.9em}
		div.title { font-weight:bold; font-size: 1.2em; }
		div.TOC {padding:2px;}
		span.TOCtop {font-weight:bold; font-size:1.6em; }
		span.TOCsmall {color #444; font-size:0.9em; }
	 	div.abs {color:#5c5c5c; font-size:0.9em}
		p {margin-bottom:5px}
		div.author {color:#000000; font-weight: normal; font-size:1em}
		span.author {color:#000000; font-weight: normal; font-size:1em}
		div.affil {color: #888888; font-size:0.9em}
		span.affil {color: #888888; font-size:0.9em}
		hr.bl {border-color:#bdbfec; padding: 0px; margin-bottom: 5px; margin-top: 5px;}
		hr.dub {border-color:#d84254 ; border-style: double; padding-bottom: 12px}
		a.entry {color:#5c5c5c; text-decoration: none; }
		a.entry:hover{ color:#FFDA00;}
	</style>""" ,

	#WORDPRESS STYLE
	"""<script type='text/javascript'>
window.addEventListener("hashchange", function () {
    window.scrollTo(window.scrollX, window.scrollY - 80);
});
</script>
<style type="text/css">
	/* 	Wordpress style */
		body { font-family: 'Open Sans', sans-serif; padding:6px 12px 8px 12px}
		div.talk  {color:#888; font-size:0.9em}
		div.timing  {color:#888; font-size:0.9em}
		div.title { font-weight:Medium; font-size: 1.3em;}
		span.TOCtop {font-weight:Medium; font-size:1.6em; }
		span.TOCsmall {color #444; font-size:0.9em; }
		div.TOC {padding:6px 12px 8px 12px;}
	 	div.abs {color:#5c5c5c; font-size:0.9em}
		div.session {font-size: 1.3em; font-color: #ffffff; background-color: #59ACEF; padding: 8px}
		div.sessiont {color:#ffffff; font-size: 1.3em; background-color: #bbdafa; padding: 8px}
		div.sessionp {color:#ffffff; font-size: 1.3em; background-color: #f1c7bc; padding: 8px}
		div.author {color:#000000; font-weight: normal; font-size:1em;}
		span.author {color:#000000; font-weight: normal; font-size:1em;}
		div.affil {color: #888888; font-size:0.9em}
		span.affil {color: #888888; font-size:0.9em}
	hr.bl {border-color:#bdbfec}
	hr.dub {border-color:#d84254 ; border-style: double; padding-bottom: 12px}
		a.entry {color:#5c5c5c; text-decoration: none; }
		a.entry:hover{ color:#FFDA00;}
</style>
<head> <meta charset="utf-8" /> <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
		<title>eDSBS Abstracts</title>
	</head>
""",    # COLUMN CSS FORMAT
'''<html lang="en">
	<style type="text/css">
	/* 	timecolumn style */
		body { font-family: 'Open Sans', sans-serif; padding:12px 24px 12px 24px}
		div.subsession {text-transform:uppercase; font-size:.8em ;
			background-color: #EEE; padding: 10px 0px 10px 6px}

		span.room  {color:#888; font-weight: normal; text-transform:uppercase; font-size:0.7em }
		#caracol{color:#600; font-weight: normal;}
		#aguamarina{color:#006; font-weight: normal;}
		span.title  {font-size:1em; font-weight: bold;}
		div.event {
		  padding-left: 42px ;
		  text-indent: -42px ;
		  padding-right: 12px;
			font-weight: bold;
			font-size: .9em;
		}

		div.row { display: flex;}
		div.column { flex: 50%; padding: 0px 8px; }

		div.breakstyle {font-size: 1.2em; color: #222; padding: 12px 8px; background-color:#C9DFB5}

		div.day {font-size: 1.2em; color: #222;  background-color: #59ACEF; padding: 8px}
		span.author {font-weight: normal; font-size:0.7em}

		hr.bl {border-color:#bdbfec; padding: 0px; margin-bottom: 5px; margin-top: 5px;}
	</style>

''' ,
# USED BY TIMETABLE
'''
<!DOCTYPE html>
<html lang="en">
	<style type="text/css">
	/* 	tableformatted style */
	*{font-size:12pt;}

	/* Items for session TOC */
	.TOCtop {font-weight:bold; font-size:1.6em; text-align: center;}
	.TOCsmall {color: #444; font-size:0.8em; text-align: center;}
	div.TOC {padding:12px 0px;}
	hr.bl {border-color:#bdbfec; padding: 0px; align-items: flex-start ; margin-bottom: 5px; margin-top: 5px; width: 90%;}
	.tochead {
		padding: 6px 8px;
		text-transform: uppercase;
		color:#000000;
	}

	.tocrow {
		padding: 18px 12px;
		border-bottom: white solid 2px;
		vertical-align: middle;
	}

	.tabletocsession {
		padding: 8px 12px;
		font-weight: bold;
/* 		color: #4a7899; */
	}
	.tabletocdays {
		padding: 8px 12px;
	}

	.tableroom {
		padding: 8px 12px;
		text-transform: uppercase;
		font-size: 1.2 em ;
		width: 20%;
	}


	body {
		font-family: 'Open Sans', sans-serif;
/* 		padding:12px 24px 12px 24px; */

		}
			/* TODO: every other row border
				fix padding on times
				make sure about cascading text sizes
			*/

	table {
		border-collapse: collapse;
	    width: 90%;
	    background-color: #ffffff;
	}

	td.ttalk {
	    padding: .6em 1em .4em .6em;
	    text-align: left;
	    border-color: #c0bee9;
	    border: 1px;
/* 	    vertical-align: top; */
	 }

    td.time_san{
	    color:#222;
/* 	    border-left: 4px solid white */
/*	    border-left: 2px solid rgba(129,126,154,0.1)    */
	}

	tr {
		vertical-align: top;
		border-bottom: 1px solid #ddd;
	}

/*
	tr:nth-child(even) {
		background-color: #f1f1fa;
	}
*/



	.tday {content-align: left;
		color:#000000;
		font-weight: normal;
		padding: 24px;
		font-size: 1.8em;
		text-align: center;
		background-color: #ffffff;
		letter-spacing:0.25em;
		/*background-color: #59ACEF*/
	}

	td.tevent {
		padding: 1em .8em;
		font-weight: bold;
/* 			vertical-align: middle; */
		border-bottom: 2px solid #fff;
		color:#000000;
	}
	td.etime {
		padding-right: .3em;
		padding-top: 1em;
		width: 8%;
		text-align: right;
		font-weight: normal;
		border-bottom: 2px solid #fff;
		color:#000000;
	}

	 .tevent.event_bg{ background-color: #f1c7bc; }
	 .tevent.eventp_bg{	background-color: #FFDA00; }

	.subsession {
		text-transform:uppercase;
		font-size: 1.1em ;
		padding: .9em .6em .8em 1em;
		/* top right bottom left */
		/*border-bottom: solid 10px rgba(0,0,0,.1); */
	}
	.subgap{
		padding-top:.5em;
	}

	.ttalk{
		padding-top: .85em;
		width: 40%;
		padding-bottom: .4em;
		padding-top: .8em;
	}

	.dtitle  {
		/* space before*/
		font-size:1em;
		font-weight:bold;
		color: #4a74ae;
	}

	.dauthor{
		padding: .5em 0em 0em 0em;
		font-size: 0.9em;
		font-weight: normal;
	}
	.dauthor{
		padding: .5em 0em 0em 0em;
		font-size: 0.9em;
		font-weight: normal;
	}

	span.firstauthor{
		font-size: 0.9em;
		font-weight: bold;
		color:#000000;
	}
	span.firstaffil{
		font-size: 0.9em;
		font-weight: normal;
		color:#222;
	}

	span.otherauthors{
		font-size: 7pt;
		font-weight: normal;
		color:#000000;
	}

	.under{
		font-size: 7pt;
		font-weight: normal;
		color:#000000;
		text-decoration : underline;
	}
	.ttime {
		padding-right: .3em;
		padding-top: 1.2em;
		width: 8%;
		text-align: right;
		font-weight: normal;
		color:#000000;

	}
	/*
	.ttime:first{
		padding-top: 2em;
	}
	*/

	.caracol {background-color: #EBCFAA;
		 color:#222; }
	.aguamarina {background-color: #bbdafa;
		color: #222; }
	.textaguamarina{color:#ffffff;}
	.textcaracol{color:#6b4a33;}

	</style>








''']
## ADJUST TITLES
	Top = ['''
	<head> <meta charset="utf-8" /> <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
		<title>eDSBS Abstracts</title>
	</head>
<body>
''',"",'''
	<head> <meta charset="utf-8" /> <link href="https://fonts.googleapis.com/css?family=Open+Sans@1&display=swap" rel="stylesheet">
		<title>eDSBS Timetable</title>
	</head>
	<body>
''','''
	<head> <meta charset="utf-8" /> <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
		<title>eDSBS Timetable</title>
	</head>
	<body>
''']

	print CSS[wordpress] + Top[wordpress]

def printTOC(postersonly=False):
	import datetime as dt

	Months=["NA","Jan","Feb","Mar","Apr","May","Jun",
		"Jul","Aug","Sep","Oct","Nov","Dec"]

	i = dt.datetime.now()
	UpdateString = "-".join([str(i.day), Months[i.month],str(i.year)])
	# Easier time? time.strftime("%d-%b-%Y",time.localtime())

	print '''<div class="TOC" id="toc"><span class="TOCtop">Abstracts (reverse alphabetical) &bullet; Virtual Deep-Sea Biology Symposium</span><br/>
	<span class="TOCsmall">Updated: {} &bullet; <a href="https://dsbsoc.org/conferences/edsbs/">Symposium Page</a></span><br/>
	<hr class="bl"/>'''.format(UpdateString)

	# COMMENTING OUT SESSIONS HERE - SH 27 July 2020
	# The other script will organize by session
	
	print "</div>"
	print "\n\n<!-- end session TOC -->\n\n"

def printTOCtable():
	import datetime as dt

	# REALLY SHOULD NOT BE DEFINING THIS TWICE!
#
# REMOVED SESSION LIST DEFINITION
#
	## ADJUST Session times and locatoins

	# roomcss, Session, day, room, chairs
	# The first field "aguamarina" doesn't need to be changed. That is a ref to the CSS style
	SessionDays = {
		"Biodiversity" : ["aguamarina","Thursday 09:00am-11:15am | Friday 09:15am-11:45am; 05:00pm-7:15pm" ,"Thinkiffic",  "Paris Stefanoudis, Chris Yesson, Erin Easton"]  ,
		"Chemosyn."    : ["aguamarina","Thursday 11:30am-02:30pm" ,"Thinkiffic",  "Jon Copley, Santiago Herrera"]  ,
		"Connectivity" : ["aguamarina","Friday 07:30am-09:00am" ,"Thinkiffic",  "Adrian Glover"]  ,
		"Corals"       : ["aguamarina","Thursday 07:30am-08:45am | 02:30pm-04:00pm" ,"Thinkiffic",  "Andrea Quattrini, Santiago Herrera"]  ,
		"Stewardship"  : ["aguamarina","Thursday 05:15pm-06:15pm" ,"Thinkiffic",  "Ily Iglesias"]  ,
		"Omics"        : ["aguamarina","Friday 03:30pm-04:45pm" ,"Thinkiffic",  "Julia Sigwart"]  ,
		"Disturbance"  : ["aguamarina","Thursday 04:15pm-05:15pm" ,"Thinkiffic",  "Ily Iglesias"]  ,
		"Pelagic"      : ["aguamarina","Friday 02:00pm-03:30pm" ,"Thinkiffic",  "Steven Haddock"]  ,
		"Seamounts"    : ["aguamarina","Friday 12:00pm-01:45pm" ,"Thinkiffic",  "Chong Chen"],
		"Taxonomy"     : ["aguamarina","Posters Only" ,"Posters",  ""]  ,
		"Trophic"      : ["aguamarina","Posters Only" ,"Posters",  ""]   }
		


	Months=["NA","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

	i = dt.datetime.now()
	UpdateString = "-".join([str(i.day), Months[i.month],str(i.year)])
	# Easier time? time.strftime("%d-%b-%Y",time.localtime())

	print '''<div class="TOC" id="toc"><div class="TOCtop">Session List &bullet; eDSBS - Deep-Sea Biology Virtual Symposium </div><br/>
	<div class="TOCsmall">Updated: {} &bullet; <a href="https://dsbsoc.org/conferences/edsbs/">Symposium Page</a></div><br/><br/>
	'''.format(UpdateString)

	SessionList.sort()
	print '<table>'
	print '''<tr class="tochead">
	<th class="tochead">Session</td>
	<th class="tochead">Day and Time Period GMT</td>
	</tr>'''

	for Session in SessionList:
		try:
			tocstyle,days,room,chairs = SessionDays.get(SessionDict.get(Session))
		except TypeError:
			print >> sys.stderr, "Session not found:",Session

		print '''<tr class="tocrow {0}">
		<td class="tabletocsession">{1}</td>
		<td class="tabletocdays">{2}</td>
		</tr>'''.format(tocstyle,Session,days)
	print "</table>"
	print "\n\n<!-- end table TOC -->\n\n"
	
	
	print '''
	<div class="TOC" id="toc"><br><span class="TOCtop">Daily Schedule of Talks and Events (All times GMT)</span><br/><br/>
	'''.format(UpdateString)


def printtail():
	Tail = """
</body>
</html>	"""
	print Tail


def printsession(SessionName,theformat,wordpress=False):
	AnchorName = SessionName.split(" ")[0].lower() + theformat[0]
	# Don't print [TOP] links if unless --wordpress
	TopLink= ['    ','<a href="#toc" style="font-size: 0.7em; color:#FFDA00;">[TOP]</a>'][wordpress]
	TString = '''<br><br><br><div class="session{}" id="{}"><b>{}</b> - {}   {}\t</div>
<br/>'''.format(theformat[0].lower(),AnchorName,SessionName,theformat,TopLink)

	"""Blanking this for edsbs"""
	TString = ''
	print TString


def printtalk(Fields,postersonly=False,lightning=False):
	AuthorBreak = ['<br>',''][postersonly or (lightning > 0)]
	AuthorBreak = ''

	## ABSTRACT BOOK HEADERS

	FirstFields = '{myformat} {bullet} {mydate} {bullet} {mytime}GMT {bullet} '.format(**Fields)
	# TimingFields = ['	<div class="timing">{day} • {mytime}</div>'.format(**Fields),''][postersonly]
	if postersonly or (Fields['myformat']=='Poster'):
		FirstLine = '''<br><div class="talk">POSTER {bullet} {sessionname} </div>
		'''.format(**Fields)
	else:
		if DEBUG:
			print >> sys.stderr, "Fields:",Fields
		## ADJUST  This will be the final location of the schedule document
		## For cross linking from the abstract book
		### ANCHOR FOR LINKING TO and FROM TIMETABLE
		FirstLine = '''<br><div class="talk" id="{TIMETOT}">{}{sessionname}  
		 <a href="https://dsbsoc.org/conferences/edsbs/schedule/#{TIMETOT}" style="font-size: 0.7em; color:#4a74ae;">[SCHEDULE]</a></div>
		'''.format(FirstFields,**Fields)

	# the or True is a Hack to get it to print this field — breaks lightning
	# This script doesn't support lightning talk, but those fields are left in to avoid breaking things
	#
	# *** THIS IS THE MAIN ABSTRACT BOOK
	#
	if postersonly or True:
		if Fields["myformat"] == "Event":
			print ''
			# print '''
			# {}
			# <div class="title" style="color:{stylecolor}">{mytitle}</div>
			# '''.format(FirstLine,**Fields)

		else:
			# removed <span  class="affil"> (TZ:{timezone})</span>
			print '''
			{}
			<div class="title" style="color:{stylecolor}">{escaped_title} </div>
			<span class="author">{}{escaped_authors}</span>'''.format(FirstLine,AuthorBreak,**Fields)
	else: # is lightning
		FirstFields=lightning
		thename = cleannames(Fields,True)
		print '''
		{}
	<span class="author">{authorname}</span> •
	<span class="title" style="color:{stylecolor}">{mytitle}</span>'''.format(FirstLine,FirstFields,**Fields)

	if not (postersonly):
		if Fields["myformat"] != "Event":
			print '''<div  class="affil">{escaped_coauthors}</div><br>
		<div class="abs">{escaped_abstract}</div><br>'''.format(**Fields)
			print '''<hr class="bl"/>'''

def printdayhead(Day):
	print '<hr class="bl"/>'
	TString = '''<div class="day"><b>{}</b></div> <br/>'''.format(Day)
	print TString


def printevent(Fields):

	''' Color time and room by color '''
	# TODO: SHort session names...
	if Fields["room"]=="OTHER":
		print '''<div class="breakstyle">{mytime} • <span class="title">{mytitle}</span></div>
		<br/>'''.format(**Fields)
	else:

		print '''
		<div class="event">{mytime} • <span class="title">{mytitle}</span>
		<span class="author">{authors}</span>
		</div>
		<br/>
	'''.format(**Fields)

def printcolumnstart():
	print '''<div class="row">
	  <div class="column">'''

def printcolumnnext():
	print '''</div>
	  <div class="column">'''

def printcolumnend():
	print '''</div> <!--endcol-->
	</div><!--endrow-->'''


def printtablestart():
	print '''<tr class="ttalk">
	  '''

def printtableevent(L,extratz):
	''' These are manually defined entries with details for extra events'''

	Student1 = '''		<div class="dauthor"><span class="firstauthor">Natasha Karenyi</span>&nbsp;
	  <span class="otherauthors">Lecturer, Biological Sciences, University of Cape Town, South Africa</span></div>
	<div class="dauthor"><span class="firstauthor">Séverine Martini</span>&nbsp;
	  <span class="otherauthors">Postdoctoral Researcher, Mediterranean Institute of Oceanography (MIO), France</span></div>
	<div class="dauthor"><span class="firstauthor">Chris Yesson</span>&nbsp;
	  <span class="otherauthors">Research Fellow, Institute of Zoology, Zoological Society of London, UK</span></div>'''
	  
	  
	Student2 = '''		<div class="dauthor"><span class="firstauthor">Jesse van der Grient</span>&nbsp;
	  <span class="otherauthors">Postdoctoral Researcher, Deep-sea ecology lab, University of Hawaii at Manoa, USA</span></div>
	<div class="dauthor"><span class="firstauthor">Di Tracey</span>&nbsp;
	  <span class="otherauthors">Deep-sea and Fisheries Scientist, National Institute of Water and Atmospheric Research, New Zealand</span></div>
	<div class="dauthor"><span class="firstauthor">Elva Escobar</span>&nbsp;
	  <span class="otherauthors">Professor, Universidad Nacional Autónoma de México; Instituto de Ciencias del Mar y Limnología, Mexico</span></div>'''
	  
	if L['mytitle'] == 'Student Panel 1':
		au_string = Student1
	elif L['mytitle'] == 'Student Panel 2':
		au_string = Student2
	# need to make tevent .break and .plenary bg colors
	elif L['authors']=='':
		au_string=''
	else:
		au_string = '''		<div class="dauthor">{escaped_authors}</div>'''.format(**L)
		
	eventtype = L['myformat'].lower()
	print 	'''<tr class="tevent {0}_bg">
	<td class="etime">{mytime}{2}</td>
	<td class="tevent {0}_bg" colspan="3">{mytitle}
	{1}</td>
</tr>'''.format(eventtype,au_string,extratz,**L)

def returntablesession(L):
	# add tevent styles for plenary vs break
	# Style session background by room color
	# Return the format... Make this just a span inside the cell?
	# div.subsession {text-transform:uppercase; font-size:.8em ;
	#	 background-color: #EEE; padding: 10px 0px 10px 6px}
	# sessionstyle = L['room'].lower().replace(" ","_")
	OutString = '''<div class="subsession {}" colspan="2">{} \t</div>
	<div class="subgap"> <div>\n'''.format(
	    'aguamarina', L['shortsession'])
	return OutString

def printtabletalk(L,SessionString,extratz):

	OpenString = '<tr class="ttalk">'
	timeclass = ''

## ADJUST - Here is the link to go from the timetable to the abstract book web page

   #### ADDED LINK FOR ABSTRACT FILE
	TalkString = '''	<td class="ttime{1}">{mytime}{2}</td>
	<td class="ttalk">{0}<div class="dtitle" id="{blocktot:.2f}">{escaped_title} &nbsp; 
	<a href="https://dsbsoc.org/conferences/edsbs/abstracts/#{TIMETOT}" style="font-size: 0.7em; color:#888;">[ABS]</a></div>
		<div class="dauthor">{escaped_authors}</div>
		</td>'''.format(SessionString,timeclass,extratz,**L)

	# Make this different for the last two sessions ==> if time > some value....
	# Time > 500.5 or blocktime > 520.5
	# CloseString = ['</tr>',''][(TalkRoom=="Aguamarina" and L['timetot'] > 500.5)]
	CloseString = '</tr>'
	print OpenString + TalkString + CloseString

def printtableend():
	print '''</tr> <!-- talkrow -->
	'''
def printtableday(Day):
	# Added this to make a new table for each day...
	print '''</table><table>'''
	print '''<tr class="event"><td class="tday" colspan="4">{}</td>
	</tr>'''.format(Day)

def cleanaffil(row,lightning=False):
	affilRE = re.compile(r'[\*]+')
	if row['affil']:
		coauthwaffil = affilRE.sub("",row['affil'])
		coauthwaffil = coauthwaffil.replace(" ,",",").replace("*","").replace(", ",", ").replace(",,",",").replace(", ,",",")
		if lightning:
			cleancoauthwaffil = coauthwaffil
		else:
			cleancoauthwaffil = '<span class="affil">'+ coauthwaffil + '</span>'

		cleancoauthwaffil = affilRE.sub("",cleancoauthwaffil)
		cleancoauthwaffil = cleancoauthwaffil.replace("; ","<br>")

	else:
		cleancoauthwaffil = ""
	return cleancoauthwaffil


def cleannames(row,lightning=False):
	authorRE = re.compile(r'[\(\d\)\]\[\*]+')
	if row['authorname']:
		firstauthor = authorRE.sub("",row['authorname'])
		firstauthor = firstauthor.replace(" ,",",").replace(",","").replace("*","")
		if lightning:
			cleanauthors = firstauthor
		else:
			cleanauthors = '<span class="firstauthor">'+ firstauthor + '</span><span class="otherauthors">' + ['',', '][len(row['coauthors'])>4] + row['coauthors']+'</span>'

		cleanauthors = authorRE.sub("",cleanauthors)
		cleanauthors = cleanauthors.replace(" ,",",").replace(",  ",", ").replace(",,",",").replace(", ,",",").replace("*","")

	else:
		cleanauthors = ""
	return cleanauthors


def splitcoauthors(cos):
	'''remove affiliation from comma and semicolon delimited names'''
	alist = cos.split(";")
	colist = [x.split(',')[0].rstrip() for x in alist]
	return colist

def cleannamestable(row,lightning=False):
	'updated for eDBSB'
	if len(row['coauthors']) >1:
		coauthorlist = splitcoauthors(row['coauthors'])
		coauthorfield = ", ".join(coauthorlist)
		cotext = '<span class="firstaffil">' + row["Affiliation"] + ' </span><span class="under">with</span> ' + '<span class="otherauthors">' +coauthorfield +'</span>'

	else:
		cotext =  '<span class="otherauthors">' + row["Affiliation"] +'</span>'
	# if row['authorname'] and (row['authorname'][:7] == row['coauthors'][:7]):
# 		print >> sys.stderr, "Author Overlap:", row['order'],row['authors']
#
	cleanauthors = '<span class="firstauthor">'+ row['authorname'] + '&nbsp;</span>' + cotext
	cleanauthors = cleanauthors.replace(" ,",",").replace(",,",",").replace(",,",",").replace("*","")
	# if DEBUG:
	# 	print >> sys.stderr, "AUTHORS",cleanauthors

	return cleanauthors


## USING THIS ONE FOR TIMETABLE
def parsedailytable(reader,Options):
	'''print abstractbook format'''
	## ADJUST DAYS AND DATES
	dayD = {"Weds" : "WEDNESDAY • 19 Aug","Thurs" : "THURSDAY • 20 Aug",
	"Fri" : "FRIDAY • 21 Aug"}

	daynumD = {"100":"Weds","200":"Thurs","300":"Fri"}


	LineD = {}
	## COPY SESSIONS FROM ABOVE (optional?)
# 	These are the same should not be needed...
 	'''	SessionList = [
		"Advances in taxonomy and phylogenetics",
		"Biodiversity and ecosystem functioning",
		"Chemosynthetic ecosystems",
		"Connectivity and biogeography",
		"Corals",
		"Deep-ocean stewardship",
		"Deep-sea omics",
		"Natural and anthropogenic disturbance",
		"Pelagic systems",
		"Seamounts and canyons",
		"Trophic ecology"]
	ShortSessionList = [
		"Advances in taxonomy and phylogenetics",
		"Biodiversity and ecosystem functioning",
		"Chemosynthetic ecosystems",
		"Connectivity and biogeography",
		"Corals",
		"Deep-ocean stewardship",
		"Deep-sea omics",
		"Natural and anthropogenic disturbance",
		"Pelagic systems",
		"Seamounts and canyons",
		"Trophic ecology"]

	SessionDict = dict(zip([x.lower() for x in SessionList],ShortSessionList))
'''
	if DEBUG:
		print >> sys.stderr, "## TABLE PARSER"

	for row in reader:
		# For timetable, skip posters
		if row['myformat'] in ["Talk","Event"]:
			try:
				decimaltime = float(row['TIMETOT'])
			except ValueError:
				print >> sys.stderr, "#Bad value error in parsedailytable",row
				decimaltime = 0.1
			# LineKey = (decimaltime)
			# LineKey = (decimaltime,row['room'])
			if row['myformat'] in ["Talk","Event"]:
				if DEBUG:
					print >> sys.stderr, row["myformat"]
				LineKey = (row['DAYNUM'],decimaltime)
				
				
				row['blocktot'] = decimaltime
				# if DEBUG:
				# 	print >> sys.stderr, row


				row['day'] = dayD[daynumD[row["DAYNUM"]]]
				# row['day'] = dayD.get(row['day'[:3]],row['day'])

				row["authors"] = cleannamestable(row)

				row['shortsession'] = SessionDict.get(row['sessionname'].lower(),row['sessionname'])

				LineD[LineKey] = row

				row["escaped_authors"] = unicode(row["authors"], "utf-8").encode("ascii", "xmlcharrefreplace")
				row["escaped_title"] = unicode(row["mytitle"], "utf-8").encode("ascii", "xmlcharrefreplace")
				row["escaped_coauthors"] = unicode(row["coauthors"], "utf-8").encode("ascii", "xmlcharrefreplace")
				row["escaped_abstract"] = unicode(row["abstract"], "utf-8").encode("ascii", "xmlcharrefreplace")
		
	if LineD:
		LineKeys = LineD.keys()

		LineKeys.sort()
		if DEBUG:
			print >> sys.stderr, "Sorted keys",LineKeys
		NewDay = ""
		printhead(3)  # new table header...
		# TODO: Figure room colors to match color abstract BREAKS too.

		extratz=''
		if not DEBUG and not Options.difftime:
			printTOCtable()
		elif Options.pacific:
			print 	'''<div class="TOC" id="toc"><br><span class="TOCtop">
Daily Schedule of Talks (All times Pacific: -7)</span><br/><br/>'''
			extratz = " PT"
			row["mytime"] = row["Pacific"]
		elif Options.eastern:
			print 	'''<div class="TOC" id="toc"><br><span class="TOCtop">
Daily Schedule of Talks (All times Eastern: -4)</span><br/><br/>'''
			extratz = " ET"		
			row["mytime"] = row["Eastern"]
		elif Options.japan:
			print 	'''<div class="TOC" id="toc"><br><span class="TOCtop">
Daily Schedule of Talks <br>/(All times Japan Standard: +9. After 00:01 may be next day)</span><br/><br/>'''
			extratz = " JT"
			row["mytime"] = row["Japan"]
		
		InDouble = False
		NowSession = "Nada"
		ColumnNumber = 0
		FormD = {'POS':'POSTER','TAL':'TALK','EVE':'EVENT'}


		print '''
		<table  cellpadding = "4" bgcolor="#EEE">

		'''

		for K in LineKeys:
			L = LineD[K]
			
			if Options.pacific:
				L["mytime"] = L["Pacific"]
			elif Options.eastern:	
				L["mytime"] = L["Eastern"]
			elif Options.japan:
				L["mytime"] = L["Japan"]
			
			
			if L['day'] != NewDay:
				if InDouble:
					printtableend()
					InDouble = False
				printtableday(L['day'])
				NowSession = ""
				NewDay = L['day']
			# changing from single to double

			L['sessionstyle'] = 'aguamarina'

			if L['myformat'] == "Event":
				if DEBUG:
					print >> sys.stderr, "IN EVENT"
				if InDouble:
					printtableend()
				printtableevent(L,extratz) #colspan 3
				ColumnNumber = 0
				InDouble = False
				NowSession = ""
			else:  #Must be a talk
				if DEBUG:
					print >> sys.stderr, "IN TALKS"
				# print session first
				# Don't need InDouble? Just print based on room?
				if NowSession != L['sessionname']:
					NowSession = L['sessionname']
					SessionString = returntablesession(L)
				else:
					SessionString = ""

				#ACTIVE INGREDIENT
				printtabletalk(L,SessionString,extratz)

				InDouble = True
		print '''
		</table>
		'''
		printtail()

def parsedaily(reader,Options,dayD):
	'''print abstractbook format'''
	'''FieldNames=["order","prefix","firstname","lastname","authorname","affil",
	"coauthors","mytitle","abstract","sessionname","newformat","finalformat",
	"jobtitle","preferredformat","lightning","finallightning","day","timeblock","room","mytime",'daynum','timetot','blocknum','blocktotal']
	'''

	LineD = {}

# 	SessionList = [
# 	"Advances in taxonomy and phylogeny",
# 	"Biodiversity and ecosystem functioning",
# 	"Chemosynthetic ecosystems",
# 	"Connectivity and biogeography",
# 	"Deep-sea corals",
# 	"Deep-ocean stewardship",
# 	"Deep-sea 'omics",
# 	"Natural and anthropogenic disturbance",
# 	"Pelagic systems",
# 	"Seamounts and canyons" ]
#
# ShortSessionList = [
# 	"Taxonomy",
# 	"Biodiversity",
# 	"Chemosynthetic",
# 	"Connectivity",
# 	"Corals",
# 	"Stewardship",
# 	"Omics",
# 	"Disturbance",
# 	"Pelagic",
# 	"Seamounts" ]
#
# 	SessionDict = dict(zip([x.lower() for x in SessionList],ShortSessionList))


	for row in reader:
		
		try:
			decimaltime = float(row['blocktotal'])
		except ValueError:
			decimaltime = 0.1
		LineKey = (decimaltime,row['room'])
		# LineKey = (row['day'],row["timeblock"],row['room'],decimaltime)
		row['blocktot'] = decimaltime
		row['day'] = dayD.get(row['day'[:3]],row['day'])

		row['authors'] = cleannames(row)

		row["escaped_authors"] = unicode(row["authors"], "utf-8").encode("ascii", "xmlcharrefreplace")
		row["escaped_title"] = unicode(row["mytitle"], "utf-8").encode("ascii", "xmlcharrefreplace")
		row["escaped_coauthors"] = unicode(row["coauthors"], "utf-8").encode("ascii", "xmlcharrefreplace")
		row["escaped_abstract"] = unicode(row["abstract"], "utf-8").encode("ascii", "xmlcharrefreplace")

		row['shortsession'] = SessionDict.get(row['sessionname'].lower(),row['sessionname'])

		LineD[LineKey] = row

	LineKeys = LineD.keys()
	LineKeys.sort()
	NewDay = ""
	printhead(2)
	# TODO: Figure room colors to match color abstract BREAKS too.

	InDouble = False
	TalkRoom = "Aguamarina"
	NowSession = "Nada"

	for K in LineKeys:
		L = LineD[K]

		# Take first 3 characters of TALK or POSTER
		truncform = L['finalformat'][:3]

		if K[0] > 1: # timefield is not a poster
			if L['day'] != NewDay:
				if InDouble:
					printcolumnend()
					InDouble = False
				printdayhead(L['day'])
				NowSession = ""
				NewDay = L['day']
			# changing from single to double

			L['sessionstyle'] = L['room'].lower().replace(" ","_")

			if not InDouble and truncform == "TAL":
				printcolumnstart()
				InDouble = True
				TalkRoom = L['room']

			# within double changing to new column
			elif InDouble and truncform == "TAL" and TalkRoom != L['room']:
				printcolumnnext()
				TalkRoom = L['room']

			# changing from double to single column
			elif InDouble and truncform != "TAL":
				printcolumnend()
				InDouble = False

			if InDouble and NowSession != L['sessionname']:
				print '''<div class="subsession" id="{}">{} • {}</div>'''.format(L['sessionstyle'],L['sessionname'],L['room'])
				NowSession = L['sessionname']

			printevent(L)

		elif truncform == "TAL":
			print >> sys.stderr, "ERROR in timetable?",L
	printtail()


def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-w", "--wordpress",    action="store_true", default=False, help="Output with wordpress CSS")
	parser.add_argument("-b", "--debug",        action="store_true", default=False, dest="DEBUG", help="Debug output")
	parser.add_argument("-d", "--daily",        action="store_true", default=False, help="Print as daily schedule w/o abstracts")
	parser.add_argument("-t", "--tabular",      action="store_true", default=False, help="Print daily schedule as table cells")
	parser.add_argument("-p", "--postersonly",  action="store_true", default=False, help="Print posters by themselves")
	parser.add_argument("-l", "--lightning",    action="store_true", default=False, help="Print lightning talk schedule")
	parser.add_argument("-e", "--email",        action="store_true", default=False, help="Print emails only")
	parser.add_argument("-s", "--student",      action="store_true", default=False, help="Print student poster numbers only")
	parser.add_argument("--pacific",      action="store_true", default=False, help="Print Pacific schedule")
	parser.add_argument("--eastern",      action="store_true", default=False, help="Print Eastern schedule")
	parser.add_argument("--japan",      action="store_true", default=False, help="Print Japan schedule")
	parser.add_argument("--difftime", action="store_true",help="Placeholder for detecting alt time zone request")
	parser.add_argument("Args", nargs=argparse.REMAINDER)
	options = parser.parse_args()
	return options



if __name__ == "__main__":

	Options = get_options()
	global DEBUG
	DEBUG = Options.DEBUG
	if DEBUG:
		print >> sys.stderr, "DEBUG MODE..."
		print >> sys.stderr, Options
	if not Options.Args:
			sys.exit(__doc__)
	else: # give a way to run on specified directories...
		if DEBUG:
			print >> sys.stderr, Options.Args
		FileName = Options.Args[0]
		outstring = ""
		Count = {'TALK':0,'POSTER':0,'EVENT':0,'lightning':0,'priority':0}
		lightningnum = 0
		LightningCount = False

		if Options.student:
			Options.postersonly = True
		
		Options.difftime = False
		if Options.pacific or Options.eastern or Options.japan:
			Options.difftime=True
			
		NowSession = ""
		NowFormat = ""

		AlreadyDid = True

		# In order of Poster,Talk # ADD EVENT HERE?
		FormatColor = ["#cd7670","#4a74ae"]

		BulletSymbol = ['-','&bullet;','+']
		FormD = {'POS':'POSTER','TAL':'TALK','EVE':'EVENT'}
		dayD = {"MON" : "MONDAY","TUE" : "TUESDAY",
"WED" : "WEDNESDAY","THU" : "THURSDAY",
"FRI" : "FRIDAY"}

		# ORIGINAL from SHEET
		FieldNames = ['Author', 'Affiliation', 'ShortTime', 'ID', 'Early-CareerAward',
		'StudentAward', 'MergedCoauthors', 'Title', 'Abstract', 'FinalSession',
		'AssignedFormat', 'AssignedTimeSlot', 'AssignedDate', 'DAYNUM', 'TIMETOT']

		###### IMPORTANT FIELD NAMES
		## ADJUST - You might have different columns in different rows. 
		# Fields should have same names as these because later they are ref'd by name
		# MODIFIED to fit old fields
		FieldNames = ['authorname', 'Affiliation', 'timezone', 'ID', 'Early-CareerAward',
		'StudentAward', 'coauthors', 'mytitle', 'abstract', 'sessionname',
		'myformat', 'mytime', 'mydate', 'DAYNUM', 'TIMETOT','Pacific','Eastern','Japan']

		daynumD = {"100":"Weds","200":"Thurs","300":"Fri"}

		FStream = open(FileName,'rU')


		reader = csv.DictReader(FStream, fieldnames=FieldNames, delimiter='\t')
		
		if Options.daily:
			parsedaily(reader,Options,dayD)
		elif Options.tabular:
			if DEBUG:
				print >> sys.stderr, "Parsing daily table"
			parsedailytable(reader,Options)
			print >> sys.stderr, "COPY from CODA preview to keep most formatting"

		else:
			printhead(Options.wordpress) # put True to print wordpress style
			if not Options.lightning:
				printTOC(Options.postersonly)

			for row in reader:
				LightningCount = False
				truncform = row['myformat'][:3].upper()
				row['day'] = daynumD.get(row["DAYNUM"],"")
				theformat = FormD.get(truncform,"")

				if not theformat:   #Skip rejected
					continue

				if Options.postersonly or Options.lightning:
					# row['authors'] = cleannames(row)
					# row['coauthaffil'] = cleanaffil(row) #################experiment
					row['myformat'] = ''
				else:


					row['authors'] = row['authorname'] + ", " + row['Affiliation']
					# row['coauthaffil'] = cleanaffil(row) #################experiment
					row["escaped_authors"] = unicode(row["authors"], "utf-8").encode("ascii", "xmlcharrefreplace")
					row["escaped_title"] = unicode(row["mytitle"], "utf-8").encode("ascii", "xmlcharrefreplace")
					row["escaped_coauthors"] = unicode(row["coauthors"], "utf-8").encode("ascii", "xmlcharrefreplace")
					row["escaped_abstract"] = unicode(row["abstract"], "utf-8").encode("ascii", "xmlcharrefreplace")

					if row['authorname'] and (row['authorname'][:7] == row['coauthors'][:7]):
						print >> sys.stderr, "Author Overlap:", row['order'],row['authors']

					# if row['authorname'] and len(row['affil']) < 2:
					# 	print >> sys.stderr, "Affil Missing :", row['order'],row['authors']



					# TODO: convert italics in title and abstract

					# print row SESSION header for each new session

				if (((row['sessionname'] != NowSession) or (theformat != NowFormat)) and theformat):
					if ( not Options.postersonly or (Options.postersonly and theformat == "POSTER")):
						if ( not Options.lightning or (Options.lightning and theformat == "POSTER")):
							printsession(row['sessionname'],theformat,Options.wordpress)
					NowSession = row['sessionname']
					NowFormat  = theformat
					FirstTime = False

				row['bullet'] = '&bullet;'

				if row['myformat']:
					row['stylecolor'] = FormatColor[theformat == 'TALK']
					if theformat == 'POSTER':
						lightningnum += 1
					else:
						row['day'] = dayD.get(row['day'[:3]],row['day'])
					# if Options.DEBUG:
					# 	print >> sys.stderr, "lightningnum:",lightningnum
					if (not Options.lightning and (not Options.postersonly) or (Options.postersonly and theformat == 'POSTER')) or \
						(Options.lightning and LightningCount):
							if ( not Options.student or (Options.student and row['student'] == "Y")):
								printtalk(row,Options.postersonly,lightningnum) # changed lightning here...
					if not Options.lightning and theformat:
						Count[theformat] = Count[theformat] + 1
						# Count['lightning'] = Count['lightning'] + 1 * LightningCount
						# Count['priority'] = Count['priority'] + 1 * LightningPriority
			if not Options.wordpress:
				printtail()

			if not Options.lightning:
				print >> sys.stderr, Count
		FStream.close()
